FROM amazoncorretto:11-alpine
ARG JAR_FILE=target/refactoring-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} refactoring.jar
ENTRYPOINT ["java", "-jar", "refactoring.jar"]
